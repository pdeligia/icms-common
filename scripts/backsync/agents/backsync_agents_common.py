from scripts.backsync.brokers import BasicBacksyncBroker


class BaseBacksyncAgent(object):
    """
    Base class for backsync agents
    """

    def __init__(self, backsync_broker: BasicBacksyncBroker, dry_run=False):
        self._broker = backsync_broker
        self._dry_run = dry_run

    def sync(self):
        raise NotImplementedError('Method to be overridden by subclasses')
