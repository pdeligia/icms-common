from scripts.prescript_setup import db
from scripts.set_departure_dates import ExYearWizard
from icms_orm.epr import TimeLineUser as TLU
from icms_orm.cmspeople import Person as OldDbPerson
from icmsutils.prehistory import PersonAndPrehistoryWriter
import sys, logging, sqlalchemy as sa
from datetime import datetime, timedelta, date


class AuthorBlockWizard(object):
    @staticmethod
    def main(dry_run=False):
        logging.info('Launching the script with dry_run={0}'.format(dry_run))
        cms_ids = {r[0] for r in db.session.query(OldDbPerson.cmsId).filter(sa.and_(
            OldDbPerson.status.in_(ExYearWizard.EXMEMBER_STATUSES),
            OldDbPerson.status != 'DECEASED',
            OldDbPerson.isAuthor == False,
            OldDbPerson.isAuthorBlock == False
        )).all()}
        logging.info('Found {0} former members, non-authors and unblocked'.format(len(cms_ids)))

        # here we want to have the timestamp of last TL where the user is IN
        sq = db.session.query(TLU.cmsId.label('cmsId'), sa.func.max(TLU.timestamp).label('rangeStart')).\
            filter(sa.and_(TLU.status.like('CMS%'), TLU.status.notin_(ExYearWizard.EXMEMBER_STATUSES))).\
            group_by(TLU.cmsId).subquery()

        # and now we want the earliest following TL where the user is out
        q = db.session.query(TLU.cmsId, sa.func.min(TLU.timestamp)).join(sq, TLU.cmsId == sq.c.cmsId).\
            filter(TLU.timestamp >= sq.c.rangeStart).filter(TLU.status.in_(ExYearWizard.EXMEMBER_STATUSES)).\
            group_by(TLU.cmsId)

        # those, whose first exmem timeline starts later than 2 years ago should not be blocked for now
        recent_member_ids = {r[0] for r in q.all() if r[1] > datetime.now() - timedelta(days=730)}

        logging.info('Found {0} people who were members within the past 2 years'.format(len(recent_member_ids)))

        blocked_to_be = cms_ids.difference(recent_member_ids)

        logging.info('Found {0} people that should get their authorship block back'.format(len(blocked_to_be)))

        entities_for_update = {
            p.cmsId: p for p in db.session.query(OldDbPerson).filter(OldDbPerson.cmsId.in_(blocked_to_be)).
            options(sa.orm.joinedload('history')).all()
        }

        aborted = 0

        for cms_id in sorted(blocked_to_be):
            logging.info('Former member about to get their author block reset: {0}'.format(cms_id))
            person = entities_for_update.get(cms_id)
            departure_date = person.get(OldDbPerson.exDate)
            departure_date = departure_date and datetime.strptime(departure_date, '%Y-%m-%d').date() or None
            if departure_date is None or (datetime.now().date() - departure_date).days < 730:
                # just double checking against the DB column that should be consistent with our findings (or close to)
                aborted += 1
                logging.warning('Aborted setting author block for CMS ID {0} - departure date in DB reads {1}'.format(cms_id, departure_date))
            else:
                writer = PersonAndPrehistoryWriter(db.session, person, person)
                writer.set_new_value(OldDbPerson.isAuthorBlock, True)
                writer.apply_changes(do_commit=False)
        if dry_run:
            db.session.rollback()
        else:
            db.session.commit()
        logging.info('Had to abort in {0} cases.'.format(aborted))


if __name__ == '__main__':
    AuthorBlockWizard.main(dry_run=any([x in map(str.lower, sys.argv) for x in ['dry', 'dryrun', 'dry-run']]))
