#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from scripts.prescript_setup import db, config
import logging
from icms_orm.common import EmailMessage, EmailLog
from icmsutils.mailutils import SMTP
import traceback
from icmsutils.regexutils import extract_all_email_addresses
from sqlalchemy import func


def main(smtp_class=SMTP):

    # subquery to get the last message log entry per email_id
    sq = db.session.query(EmailLog.email_id.label('email_id'), func.max(EmailLog.id).label('last_log_id')).\
        group_by(EmailLog.email_id).subquery()

    mails_to_send = db.session.query(EmailMessage, EmailLog).join(sq, sq.c.email_id == EmailMessage.id).\
        join(EmailLog, EmailLog.id == sq.c.last_log_id).\
        filter(EmailLog.status.in_({EmailLog.Status.NEW, EmailLog.Status.FAILED})).order_by(EmailMessage.last_modified).\
        limit(int(config['mailing']['bunch_size'])).all()

    logging.debug('Found %d emails to send' % len(mails_to_send))
    smtp = smtp_class()
    for msg, log in mails_to_send:
        new_log = send_email(smtp, msg)
        db.session.add(msg)
        db.session.add(new_log)
        db.session.commit()


def send_email(smtp, msg):
    mail_redirect = config['mailing']['redirect_to']
    # some info put on top of each redirected email
    redirect_header = '' if not mail_redirect else 40 * ' - ' + '\n' + \
                      'Message redirected to %s. Original recipients: \n - to: %s\n - cc: %s\n - bcc: %s' \
                      % (mail_redirect, msg.to, msg.cc, msg.bcc) + '\n' + 40 * ' - ' + '\n'

    mail_params = dict(
        subject=msg.subject.encode('utf8'),
        body=(redirect_header + msg.body).encode('utf8'),
        from_address=msg.sender,
        to_addresses=extract_all_email_addresses(mail_redirect or msg.to),
        cc_addresses=extract_all_email_addresses(mail_redirect or msg.cc),
        bcc_addresses=extract_all_email_addresses(mail_redirect or msg.bcc),
        reply_to=msg.reply_to
    )
    logging.debug('About to dispatch an email with the following params: %s' % str(mail_params))
    log = EmailLog(action=EmailLog.Action.SEND, email=msg, status=EmailLog.Status.SENT)
    try:
        assert mail_params.get('to_addresses') is not None
        smtp.send_email(**mail_params)
    except Exception as e:
        log.status = EmailLog.Status.FAILED
        logging.error('Failed to send an email #%d due to %s', msg.id, e)
        logging.error(traceback.format_exc())
    return log


if __name__ == '__main__':
    main()
