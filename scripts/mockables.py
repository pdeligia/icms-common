#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

try:
    from icmsutils.icmstest.mock import mock_date_param_name, today, date
except RuntimeError as re:
    from datetime import date
    today = date.today