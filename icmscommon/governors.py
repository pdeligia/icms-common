#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from icms_orm import OrmManager
from typing import Callable, TypeVar, Type
import abc


class DbGovernor(object):
    _db_manager_getter = lambda: None

    @classmethod
    def get_db_manager(cls) -> OrmManager:
        if cls._db_manager_getter is None:
            raise RuntimeError('DB Manager getter is not set. Perhaps some init script should have been sourced first?')
        return cls._db_manager_getter()

    @classmethod
    def set_db_manager_getter(cls, db_manager_getter: Callable[[], OrmManager]):
        cls._db_manager_getter = db_manager_getter


T = TypeVar('T', bound='ConfigGovernor')


class ConfigGovernor(abc.ABC):

    _instance = None

    @classmethod
    def set_instance(cls, governor):
        assert isinstance(governor, ConfigGovernor)
        cls._instance = governor

    @classmethod
    def get_instance(cls: Type[T]) -> T:
        if cls._instance is None:
            raise RuntimeError('ConfigGovernor instance has not been set. Perhaps some init script should have been sourced earlier on?')
        return cls._instance

    # Class method proxies for convenience
    @classmethod
    def check_test_mode(cls) -> bool:
        return cls.get_instance()._check_test_mode()

    @classmethod
    def get_db_owner_user_pass(cls) -> (str, str):
        return cls.get_instance()._get_db_owner_user(), cls.get_instance()._get_db_owner_pass()

    @classmethod
    def get_epr_db_string(cls) -> str:
        return cls.get_instance()._get_epr_db_string()

    @classmethod
    def get_db_string(cls, db: str) -> str:
        return cls.get_instance()._get_db_string(db)

    # Abstract methods here, please...

    @abc.abstractmethod
    def _check_test_mode(self) -> bool:
        pass

    @abc.abstractmethod
    def _get_db_owner_pass(self):
        pass

    @abc.abstractmethod
    def _get_db_owner_user(self):
        pass

    @abc.abstractmethod
    def _get_epr_db_string(self):
        pass

    @abc.abstractmethod
    def _get_db_string(self, db: str) -> str:
        pass
