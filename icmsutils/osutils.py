#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import commands
import os
import logging


def reload_local_mysql_db(path_to_dump, hostname, username, password=None):
    if not os.path.exists(path_to_dump):
        raise ValueError('%s dump file does not seem to exist!' % path_to_dump)

    part_a = 'cat %s'
    if path_to_dump.endswith('.gz'):
        part_a = 'gunzip -c %s'
    part_a = part_a % path_to_dump

    part_b = 'mysql -h %s -u %s ' % (hostname, username)
    if password:
        part_b += '-p%s' % password
    cmd = '%s | %s' % (part_a, part_b)
    logging.debug('About to execute the following command: %s' % cmd)
    (status, output) = commands.getstatusoutput(cmd)
    if status != 0:
        raise RuntimeError('Got status %s: %s' % (status, output))
    logging.debug('Executed with status %s (output: %s)' % (status, output))

if __name__ == '__main__':
    reload_local_mysql_db('/Users/bawey/Data/iCMS.dumps.bkp/2015/054/CMSPEOPLE.dump', 'localhost', 'icms_dev')
    # reload_local_mysql_db('/Users/bawey/Data/iCMS.dumps.bkp/2016/051/CMSPEOPLE.dump.gz', 'localhost', 'icms_dev')
