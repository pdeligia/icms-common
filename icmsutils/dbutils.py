#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import re
import icms_orm
import sqlalchemy as sa
import logging


def pg_parse_login_pass_host_db(conn_str):
    return re.search(r'://([^:]+):(\w*)@(\w*|[.\d]*)/(\w*)', conn_str).groups()


def get_changed_objects(db_session, entity_class, since_when):
    q = db_session.query(entity_class)
    return join_against_triggers_output(q, since_when).all()


def get_ref_ids_of_deleted_records(entity_class, since_when):
    ChangeLog = icms_orm.metadata.ChangeLog
    OperationValues = icms_orm.metadata.OperationValues
    table_name = entity_class.__table__.name
    schema_name = entity_class.__table__.schema
    q = ChangeLog.session().query(sa.func.coalesce(ChangeLog.ref_id, ChangeLog.ref_hash_id)).filter(sa.and_(
            ChangeLog.ref_schema == schema_name,
            ChangeLog.ref_table == table_name,
            ChangeLog.last_update >= since_when,
            ChangeLog.operation == OperationValues.DELETE
        ))
    return q.all()


def join_against_triggers_output(query, since_when):
    """
    :param query: An SQLAlchemy query object
    :param since_when: only pick rows updated after that date
    :return: query object joined against the tables where triggers saved update timestamps
    """
    from icms_orm import metadata
    _join_criteria = []

    ChangeLog = icms_orm.metadata.ChangeLog
    OperationValues = icms_orm.metadata.OperationValues

    for entity_class in set(query.entities):
        p_keys = entity_class.primary_keys()
        table_name = entity_class.__table__.name
        schema_name = entity_class.__table__.schema

        if len(p_keys) == 0:
            logging.warn('Table %s seems to have no primary keys.' % table_name)
            continue

        if len(p_keys) == 1 and p_keys[0].type.python_type in (int,):
            pk = p_keys[0]
            _join_criteria.append(sa.and_(pk == ChangeLog.ref_id, ChangeLog.ref_schema == schema_name, ChangeLog.ref_table == table_name))
        else:
            # need to check against hashes obtained like so: md5(concat_ws('|', NEW.id, NEW.relid))
            _join_criteria.append(sa.and_(ChangeLog.ref_hash_id == sa.func.md5(sa.func.concat_ws('|', *p_keys)),
                                            ChangeLog.ref_schema == schema_name,
                                            ChangeLog.ref_table == table_name))

    # now we apply the join and filters so that we don't join in the ChangeLog multiple times
    query = query.join(ChangeLog, sa.or_(*_join_criteria)).filter(ChangeLog.last_update >= since_when)

    return query
