"""
Common code for sending emails from/to CERN via its SMTP servers for all new iCMS services.
"""
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import logging
import smtplib
from email.mime.text import MIMEText
import email.header

__author__ = 'Miguel Ojeda'
__copyright__ = 'Copyright 2017, CERN CMS'
__credits__ = ['Miguel Ojeda', 'Andreas Pfeiffer']
__license__ = 'Unknown'
__maintainer__ = 'Andreas Pfeiffer'
__email__ = 'andreas.pfeiffer@cern.ch'


class SMTP(object):
    """
    Class used for sending emails from/to CERN via its SMTP servers.
    """

    def __init__(self, password=None, server=None):
        """
        If password is None, the anonymous SMTP server, cernmx.cern.ch will be used.
        Otherwise, the normal SMTP server, smtp.cern.ch, will be used.

        The server can also be overriden if it is not None.

        STARTTLS authentication will be used if password is not None.

        Note that the anonymous SMTP server has restrictions, see:
        https://espace.cern.ch/mmmservices-help/ManagingYourMailbox/Security/Pages/AnonymousPosting.aspx
        """

        self.password = password

        if self.password is None:
            self.server = 'cernmx.cern.ch'
        else:
            self.server = 'smtp.cern.ch'

        if server is not None:
            self.server = server

    def __str__(self):
        return 'SMTP %s' % self.server

    def send_email(self, subject, body, from_address, to_addresses, cc_addresses=(), bcc_addresses=(), reply_to=None):
        """
        Sends an email.
        Note that toAddresses and [b]ccAddresses are lists of emails.
        """

        logging.debug('%s: Email from %s with subject %s: Preparing...', self, from_address, repr(subject))
        text = MIMEText(body.decode() if isinstance(body, bytes) else body)
        text['Subject'] = email.header.Header(subject, charset='utf8')
        text['From'] = from_address
        if reply_to:
            text['reply-to'] = reply_to

        all_addrs = set(to_addresses)
        text['To'] = ', '.join(to_addresses)
        if cc_addresses and len(cc_addresses) > 0:
            text['Cc'] = ', '.join(cc_addresses)
            all_addrs |= set(cc_addresses)
        if bcc_addresses and len(bcc_addresses) > 0:
            text['Bcc'] = ', '.join(bcc_addresses)
            all_addrs |= set(bcc_addresses)

        logging.debug('%s: Email from %s with subject %s: Connecting...', self, from_address, repr(subject))

        smtp = smtplib.SMTP(self.server)
        if self.password is not None:
            logging.debug('%s: Email from %s with subject %s: Logging in...', self, from_address, repr(subject))
            smtp.starttls()
            smtp.ehlo()
            smtp.login(from_address, self.password)

        logging.debug('%s: Email from %s with subject %s: Sending...', self, from_address, repr(subject))
        smtp.sendmail(from_address, all_addrs, text.as_string())

        smtp.quit()

