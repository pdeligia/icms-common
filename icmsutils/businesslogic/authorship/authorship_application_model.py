import sqlalchemy as sa
from icms_orm.epr import TimeLineUser as TLU
from icmsutils.businesslogic import servicework as epr
from icms_orm.cmspeople import Person, PersonHistory, MemberActivity, MemberStatusValues
from icmsutils.prehistory import PrehistoryModel
from datetime import date


class AuthorshipApplicationModel(object):
    """
        Collects all the information describing one's authorship application, including:
        - the start date
        - EPR reliefs (6-X or others)
        - applicant dues assigned over the years
        - work done over the years
        """
    _cache = {}

    @classmethod
    def store_in_cache(cls, type, key, data):
        """
                    Can use this to bypass some DB calls during the object initialisation.
                    :param type:
                    :param data:
                    :return:
                    """
        cls._cache[type] = cls._cache.get(type, {})
        cls._cache[type][key] = data

    @classmethod
    def get_from_cache(cls, type, key):
        if type in cls._cache and key in cls._cache.get(type):
            # print 'CACHE HIT: {0}, {1}, {2}'.format(type.__name__, key, cls._cache.get(type).get(key) is None)
            return cls._cache.get(type).get(key)
        # print 'CACHE MISS: {0}, {1}'.format(type.__name__, key)
        return None


    @classmethod
    def wipe_preloaded_info(cls):
        """
        Erases class cache
        """
        cls._cache = {}

    def __init__(self, cms_id):
        # getting the stats of applicant due

        _row = self.get_from_cache(TLU, cms_id)
        if _row is None:
            _row = TLU.session().query(
                sa.func.min(TLU.year), sa.func.max(TLU.year), sa.func.sum(TLU.dueApplicant),
                sa.func.sum(TLU.dueAuthor)). \
                filter(sa.and_(TLU.dueApplicant > 0, TLU.cmsId == cms_id)).group_by(TLU.cmsId).one_or_none()

        (self._first_due_year, self._last_due_year, self._total_due, self._total_author_due) = _row or (None, None, 0.0, None)

        # getting the work stats
        _shift_info = epr.get_person_shifts_done(cms_id=cms_id, year=range(2010, date.today().year + 1), db_session=TLU.session())
        _work_info = epr.get_person_worked(cms_id=cms_id, year=range(2010, date.today().year + 1), db_session=TLU.session())

        # swap with dicts in case just a plain 0 is returned
        _shift_info = _shift_info if isinstance(_shift_info, dict) else {date.today().year: 0.0}
        _work_info = _work_info if isinstance(_work_info, dict) else {date.today().year: 0.0}


        self._person = self.get_from_cache(Person, cms_id)
        self._history = self.get_from_cache(PersonHistory, cms_id)
        self._activity = self.get_from_cache(MemberActivity, cms_id)

        if any([x is None for x in (self._person, self._history, self._activity)]):
            self._person, self._history, self._activity = Person.session().query(Person, PersonHistory, MemberActivity) \
                .join(PersonHistory, Person.cmsId == PersonHistory.cmsId) \
                .join(MemberActivity, MemberActivity.id == Person.activityId) \
                .filter(Person.cmsId == cms_id).one()

        self._prehist_model = PrehistoryModel(cms_id=cms_id, person=self._person, history=self._history)

        # todo: check if first year of due is the first year of application!
        # todo: can only sum the work done during the application - easy for open ones, tougher for closed!
        self._epr_done_prior = None
        self._epr_done_during = None
        if self.app_start_date:
            self._epr_done_during = sum(_work_info.get(_year, 0) + _shift_info.get(_year, 0) for _year in range(self.app_start_date.year, 2020))
            self._epr_done_prior = sum(_work_info.get(_year, 0) + _shift_info.get(_year, 0) for _year in range(min(_shift_info.keys() + _work_info.keys()), self.app_start_date.year))

    @property
    def app_start_date(self):
        return self._prehist_model.get_authorship_application_start_date()

    @property
    def epr_done_prior(self):
        return self._epr_done_prior

    @property
    def epr_done_during(self):
        return self._epr_done_during

    @property
    def total_reliefs(self):
        return min(self.base_target, self.epr_done_prior)

    @property
    def total_due(self):
        return self._total_due

    @property
    def first_year_of_due(self):
        return self._first_due_year

    @property
    def last_year_of_due(self):
        return self._last_due_year

    @property
    def is_suspended(self):
        return self._prehist_model.person.get(Person.isAuthorSuspended)

    @property
    def is_blocked(self):
        return self._prehist_model.person.get(Person.isAuthorBlock)

    @property
    def is_author(self):
        return self._prehist_model.person.get(Person.isAuthor)

    @property
    def is_over(self):
        pass

    @property
    def is_successful(self):
        pass

    @property
    def base_target(self):
        return epr.get_annual_applicant_due(self.app_start_date.year) if self.app_start_date else None

    @property
    def total_due_and_reliefs(self):
        return self.total_due + self.total_reliefs

    #todo: some applications get interrupted by things like: a) departure from CMS b) changing activity c) suspension d) changing institutes?
    #todo: and what if they have moved institutes and changed for something that ain't CMS member?
    @property
    def would_be_applicant_if_blocked(self):
        if self.is_suspended or \
                not MemberActivity.does_activity_name_trigger_application(self.activity_name) or \
                self.cms_status != MemberStatusValues.PERSON_STATUS_CMS:
            # not applicant because: suspended OR not CMS status OR not in the right activity
            return False
        return True

    @property
    def activity_name(self):
        return self._activity.get(MemberActivity.name)

    @property
    def cms_status(self):
        return self._person.get(Person.status)

    @property
    def desc(self):
        return 'CMS ID {cms_id}, {activity}'.format(cms_id=self._person.get(Person.cmsId), activity=self.activity_name)

    @property
    def has_ever_been_author(self):
        return self._total_author_due > 0



































