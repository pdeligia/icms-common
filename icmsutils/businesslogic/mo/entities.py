from icms_orm.common import MoStatusValues, MO
from icmsutils.businesslogic.people import CmsUser, CmsTeam
from datetime import datetime


class MoState(object):
    __followup_names = {
        None: {MoStatusValues.PROPOSED, MoStatusValues.APPROVED_LATE, MoStatusValues.APPROVED}.union(
            set(MoStatusValues.all_free())),
        MoStatusValues.PROPOSED: {MoStatusValues.APPROVED, MoStatusValues.REJECTED, MoStatusValues.APPROVED_SWAPPED_IN},
        MoStatusValues.APPROVED: {MoStatusValues.SWAPPED_OUT},
        MoStatusValues.APPROVED_LATE: {MoStatusValues.SWAPPED_OUT},
        MoStatusValues.REJECTED: {MoStatusValues.PROPOSED, MoStatusValues.APPROVED_SWAPPED_IN}.union(
            set(MoStatusValues.all_free())),
        MoStatusValues.CANCELLED: {MoStatusValues.PROPOSED, MoStatusValues.APPROVED_SWAPPED_IN}.union(
            set(MoStatusValues.all_free())),
        # terminating a swap is a bit of a pickle. can a swapped-in be swapped out? cancelling the swap?
        MoStatusValues.APPROVED_SWAPPED_IN: {MoStatusValues.SWAPPED_OUT, MoStatusValues.CANCELLED},
        # when cancelling the swap, what happens to the original mo listed? should go back to approved probably...
        MoStatusValues.SWAPPED_OUT: {MoStatusValues.APPROVED, MoStatusValues.APPROVED_LATE},
    }

    def __init__(self, name: str):
        self.__name = name

    @property
    def name(self) -> str:
        return self.__name

    @property
    def possible_next_steps(self) -> ['MoState']:
        return [MoState(value) for value in MoState.__followup_names.get(self.name)]

    def can_be_followed_by(self, another: 'MoState'):
        return another.name in self.__followup_names.get(self.name, [])

    @classmethod
    def get_possible_initial_states(cls):
        return [MoState(value) for value in MoState.__followup_names.get(None)]



class MoEntry(object):
    def __init__(self, cms_id: int, year: int, state: MoState, timestamp: datetime, inst_code: str = None):
        self.__cms_id = cms_id
        self.__year = year
        self.__state = state
        self.__timestamp = timestamp
        self.__inst_code = inst_code

    @classmethod
    def from_dao(cls, mo: MO) -> 'MoEntry':
        if mo is None:
            return None
        return cls(cms_id=mo.cms_id, year=mo.year, state=mo.status, timestamp=mo.timestamp, inst_code=mo.inst_code)

    @property
    def cms_id(self) -> int:
        return self.__cms_id

    @property
    def year(self) -> int:
        return self.__year

    @property
    def state(self) -> MoState:
        return self.__state

    @property
    def timestamp(self) -> datetime:
        return self.__timestamp

    @property
    def inst_code(self) -> str:
        return self.__inst_code
