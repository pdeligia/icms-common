"""
Initial idea: fill the missing prehistories using the info inferred from EPR timelines
"""
import re

from icmsutils.prehistory import prehistory_parser
from icmsutils.prehistory import PrehistoryParser, PrehistoryModel, PrehistoryEntry, PrehistoryChange
from icms_orm.cmspeople import PersonHistory, Person, MemberActivity
from icms_orm.epr import TimeLineUser as TLU, Category
import logging


class PrehistoryPatcher(object):
    """
    Somehow this class needs to correlate the changes that can be found in the EPR timelines with
    those extracted from the text history.
    Afterwards look for matches and missing ones.
    """
    def __init__(self, person, person_history, epr_time_lines):
        self._person, self._history, self._tlus = (person, person_history, epr_time_lines)
        self._ph_parser = PrehistoryParser(history_string=self._history.get(PersonHistory.history), person_obj=person)
        self._ph_events = []
        self._epr_events = []
        self._missing_events = []
        self._find_missing_events()

    @classmethod
    def new_instance(cls, cms_id, person=None, history=None, epr_time_lines=None):
        ssn = Person.session()
        _person, _history = person and history and (person, history) or ssn.query(Person, PersonHistory).join(
            PersonHistory, PersonHistory.cmsId == Person.cmsId).filter(Person.cmsId == cms_id).one()
        _epr_time_lines = epr_time_lines or ssn.query(TLU).filter(TLU.cmsId == cms_id).all()
        return cls(person=_person, person_history=_history, epr_time_lines=_epr_time_lines)

    def _store_ph_events(self):
        self._ph_events = self._ph_parser.get_events()
        logging.debug('Found %d pre-history events' % len(self._ph_events))

    def _store_epr_events(self):
        tlus = sorted(self._tlus, cmp=lambda a, b: (a.get(TLU.timestamp) - b.get(TLU.timestamp)).days)
        for i, tlu in enumerate(tlus):
            if i + 1 == len(tlus):
                break
            next = tlus[i + 1]
            # we'll populate them if we find something to populate them with
            old_values = {}
            new_values = {}
            for param in [TLU.instCode, TLU.status, TLU.category]:
                if tlu.get(param) != next.get(param):
                    old, new = [PrehistoryPatcher.__convert_epr_value_to_icms(param, o.get(param)) for o in [tlu, next]]
                    logging.debug('On %s value of %s changed from %s to %s!' % (next.get(TLU.timestamp), param, old, new))
                    phe_attr = PrehistoryPatcher.__get_change_event_param(param)
                    old_values[phe_attr] = old
                    new_values[phe_attr] = new
            if old_values and new_values:
                # inserting at the top to mimic the expected sorting of PH Events array
                self._epr_events.insert(0, PrehistoryChange(date=next.get(TLU.timestamp).date(), old_values=old_values, new_values=new_values))
        logging.debug('Found %d EPR events' % len(self._epr_events))

    def _is_reflected_in_pre_history(self, event):
        for counterpart in self._ph_events:
            if abs((event.date - counterpart.date).days) < 20:
                logging.debug('Something close...')
                # if all the keys e
                for attr in event.old_values.keys():
                    if attr not in (counterpart.old_values or {}) and attr not in (counterpart.new_values or {}):
                        # the attribute isn't even mentioned in the change event
                        return False
                    else:
                        if counterpart.old_values:
                            if counterpart.old_values.get(attr) != event.old_values.get(attr):
                                # some different old value
                                return False
                        if counterpart.new_values:
                            if counterpart.new_values.get(attr) != event.new_values.get(attr):
                                # some different new value
                                return False
                return True
        return False

    def _find_missing_events(self):
        self._store_ph_events()
        self._store_epr_events()

        for event in self._epr_events:
            if not self._is_reflected_in_pre_history(event):
                self._missing_events.append(event)
        logging.debug('Found %d EPR events missing from the pre-history' % len(self._missing_events))

    def get_missing_events(self):
        return self._missing_events

    def get_patched_history(self, actor_cms_id=0):
        raw_lines = self._ph_parser.get_raw_lines()
        date_pattern = pattern = r'(?P<date>\d{1,2}/\d{1,2}/\d{4})';
        for event in self._missing_events:
            for idx, line in enumerate(raw_lines):
                m = re.search(date_pattern, line)
                if m:
                    line_date = PrehistoryEntry.make_date(m.group('date'))
                    if event.date > line_date:
                        raw_lines.insert(idx, event.to_line(cms_id=actor_cms_id))
                        break;
        return PrehistoryParser.merge_lines_into_raw_history(raw_lines)

    def do_patch_prehistory(self, actor_cms_id=0):
        patched = self.get_patched_history(actor_cms_id)
        self._history.set(PersonHistory.history, patched)
        PersonHistory.session().add(self._history)
        PersonHistory.session().commit()

    @staticmethod
    def __get_change_event_param(tlu_param):
        return {TLU.category: PrehistoryEntry.Attr.CMS_ACTIVITY, TLU.status: PrehistoryEntry.Attr.CMS_STATUS,
                TLU.instCode: PrehistoryEntry.Attr.INST_CODE}.get(tlu_param)

    @staticmethod
    def __convert_epr_value_to_icms(param, value):
        if param == TLU.category:
            epr_cats = {c[0]: c[1] for c in Category.session().query(Category.id, Category.name).all()}
            # Ultimately redundant as iCMS events store the activity name, not code
            # icms_cats = {c[0]: c[1] for c in MemberActivity.session().query(MemberActivity.name, MemberActivity.id).all()}
            # return icms_cats.get(epr_cats.get(value, None), None)
            return epr_cats.get(value, None)
        return value


# class PrehistoryLineInserter(object):
#     def __init__(self, history_string, change_object, actor_cms_id=0):
#         self._history = history_string
#         self._change = change_object
#         self._cms_id = actor_cms_id
#
#     def insert_missing_lines(self):
#
#
#         pass
#
#     def get_history_string(self):
#         return self._history
