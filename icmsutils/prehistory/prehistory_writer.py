from datetime import datetime as dt
from icms_orm.cmspeople import Person, PeopleFlagsAssociation, MoData, User
import logging
from enum import Enum


class PersonAndPrehistoryWriter(object):
    """
    Takes care of changing legacy DB objects.
    """

    class Mode(Enum):
        ICMS_FOR = 1
        DIRECT = 2

    def __init__(self, db_session, object_person, actor_person, mode=None, date=None, mo=None, user=None):
        self._ssn = db_session
        self.object_person = object_person
        self.actor = actor_person
        self.changes = dict()
        self.flags_to_grant = set()
        self.flags_to_revoke = set()
        self.mode = mode
        self.owned_flags = {f.id for f in self.object_person.flags}
        self.date = date or dt.now().date()
        self.mo_data = mo
        self.user_data = user

    def get_mode_string(self):
        date_str: str = (self.date or dt.now()).strftime('%d/%m/%Y')
        if self.mode == PersonAndPrehistoryWriter.Mode.DIRECT:
            return f'{self.actor.loginId or self.actor.niceLoginId}_{date_str}'
        else:
            return f'iCMSfor{self.object_person.cmsId}_{date_str}'

    @classmethod
    def get_unsupervised_instance(cls, object_person, mode=Mode.ICMS_FOR):
        return PersonAndPrehistoryWriter(Person.session(), object_person, None, mode)

    @property
    def ssn(self):
        return Person.session()

    def set_new_value(self, attribute, value):
        """
        :param attribute: reference a field from Person class
        :param value: the new value
        :return:
        """
        self.changes[attribute] = value

    def grant_flag(self, flag_id):
        if flag_id in self.flags_to_revoke:
            self.flags_to_revoke.remove(flag_id)
        elif flag_id not in self.owned_flags:
            self.flags_to_grant.add(flag_id)

    def revoke_flag(self, flag_id):
        if flag_id in self.flags_to_grant:
            self.flags_to_grant.remove(flag_id)
        elif flag_id in self.owned_flags:
            self.flags_to_revoke.add(flag_id)

    def __append_history(self, change_info):
        self.object_person.history.history = '\r\n'.join(
            [f'{self.get_mode_string()}-{change_info}', self.object_person.history.history])

    def __get_subject(self, field):
        if field.class_ == Person:
            return self.object_person
        elif field.class_ == MoData:
            if not self.mo_data:
                self.mo_data = self.__retrieve_subject_from_db(field)
            return self.mo_data
        elif field.class_ == User:
            if not self.user_data:
                self.user_data = self.__retrieve_subject_from_db(field)
            return self.user_data

    def __retrieve_subject_from_db(self, field):
        return self.ssn.query(field.class_).filter(field.class_.cmsId == self.object_person.cmsId).one()

    def apply_changes(self, do_commit=False):
        """
        Modifies
        :param do_commit:
        :return:
        """

        if hasattr(self, '_safety_token'):
            raise Exception('Writer instance already expended!')

        for field, new_value in self.changes.items():
            subject = self.__get_subject(field)

            self.__append_history('%s:%s' % (field.name, str(getattr(subject, field.key) or '')))
            setattr(subject, field.key, new_value)

            if field == Person.isAuthorSuspended:
                setattr(self.object_person, (Person.dateAuthorSuspension if new_value else Person.dateAuthorUnsuspension).key,
                        self.date)
            elif field == Person.isAuthorBlock:
                setattr(self.object_person, (Person.dateAuthorBlock if new_value else Person.dateAuthorUnblock).key,
                        self.date)

        for flag_id in self.flags_to_grant:
            new_flag = PeopleFlagsAssociation()
            (new_flag.cmsId, new_flag.flagId) = (self.object_person.cmsId, flag_id)
            logging.info(
                f'New flag assoc for {new_flag.cmsId}: {new_flag.flagId}')
            self.ssn.add(new_flag)
            self.__append_history(f'added:{flag_id}')

        for flag_id in self.flags_to_revoke:
            self.ssn.query(PeopleFlagsAssociation).filter(PeopleFlagsAssociation.cmsId == self.object_person.cmsId).filter(
                PeopleFlagsAssociation.flagId == flag_id).delete()
            self.__append_history(f'removed:{flag_id}')

        for x in [y for y in (self.object_person, self.mo_data, self.user_data) if y]:
            self.ssn.add(x)

        self.ssn.add(self.object_person.history)
        if do_commit:
            self.ssn.commit()
        self._safety_token = 'Writer used already'
