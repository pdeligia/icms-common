import re
from datetime import datetime as dt
from icms_orm.cmspeople import Institute, Person
from icmsutils.businesslogic.superseders import SupersedersResolver
from icmsutils.prehistory import ActivityResolver
from icms_orm.common import PrehistoryTimeline


class PrehistoryEntry(object):

    class Attr(object):
        CMS_STATUS = 'status_cms'
        CMS_ACTIVITY = 'activity_cms'
        INST_CODE = 'inst_code'
        CERN_STATUS = 'cern_status'
        LOCAL_PERCENT = 'local_percent'
        PROJECT = 'project'

        @classmethod
        def reverse_resolve(cls, attr):
            return {cls.CMS_STATUS: Person.status.name, cls.CMS_ACTIVITY: Person.activityId.name,
                    cls.INST_CODE: Person.instCode.name, cls.PROJECT: Person.project.name}.get(attr, attr)

        __alter_names = None

        @classmethod
        def time_line_attr(cls, attr):
            return {cls.CMS_ACTIVITY: PrehistoryTimeline.activity_cms, cls.CMS_STATUS: PrehistoryTimeline.status_cms,
                    cls.INST_CODE: PrehistoryTimeline.inst_code, cls.CERN_STATUS: None,
                    cls.LOCAL_PERCENT: None, cls.PROJECT: None}.get(attr)

        @classmethod
        def resolve(cls, s):
            for cnd in [cls.CMS_ACTIVITY, cls.CMS_STATUS, cls.INST_CODE, cls.LOCAL_PERCENT, cls.CERN_STATUS, cls.PROJECT]:
                if all([x in s.lower() for x in cnd.split('_')]) and len(cnd) - cnd.count('_') <= len(s) <= len(cnd):
                    return cnd
            # check alternative names list
            for key, names in cls.__alter_names.items():
                if s.lower() in names:
                    return key
            return s

    pattern = None

    @classmethod
    def from_line(cls, line):
        match = cls.pattern and re.search(cls.pattern, line) or None
        result = None
        if match:
            result = cls.from_match(match)
            if result:
                return result
        # print('line %s not of class %s. failed to find: %s' % (line, cls.__name__, cls.pattern))
        if not result:
            for subclass in cls.__subclasses__():
                result = subclass.from_line(line)
                if result:
                    return result
        return None

    @staticmethod
    def make_date(s):
        try:
            return dt.strptime(s, '%d/%m/%Y').date()
        except Exception as e:
            return dt.strptime(s, '%m/%d/%Y').date()

    def to_line(self, cms_id):
        from icmsutils.prehistory import PersonAndPrehistoryWriter
        header = PersonAndPrehistoryWriter.Mode.ICMS_FOR(Person.from_ia_dict({Person.cmsId: cms_id}), self.date)
        body = ';'.join(['%s:%s' % (PrehistoryEntry.Attr.reverse_resolve(key), val) for key, val in self.old_values.items()])
        return '%s-%s' % (header, body)

# todo: comment why this is here as it's apparently needed but it looks as far from obvious as it possibly can
PrehistoryEntry.Attr._Attr__alter_names = {PrehistoryEntry.Attr.CMS_ACTIVITY: {'activity'}}


class PrehistoryChange(PrehistoryEntry):

    pattern = r'(?P<who>)_(?P<date>\d{1,2}/\d{1,2}/\d{4})-(?P<what>(?!fromPH:).+)'
    pattern_details_a = r'(?P<param>\w+):to(?P<new_val>.+)from(?P<old_val>.*)'
    pattern_details_b = r'(?P<param>\w+):(?P<old_val>[^;]*)?'

    def __init__(self, date, old_values, new_values=None):
        self.date = date
        self.old_values = old_values
        self.new_values = new_values

    @classmethod
    def from_match(cls, match):
        date = PrehistoryEntry.make_date(match.group('date'))
        what = match.group('what')
        ms = re.findall(PrehistoryChange.pattern_details_a, what) or re.findall(PrehistoryChange.pattern_details_b, what)
        if not ms:
            return None

        old_values = dict()
        new_values = dict()

        for m in ms:
            param = PrehistoryEntry.Attr.resolve(m[0])
            old_val = PrehistoryChange.remap_value(m[-1], param)
            new_val = len(m) > 2 and PrehistoryChange.remap_value(m[1], param) or None
            old_values[param] = old_val
            if new_val:
                new_values[param] = new_val

        return cls(date=date, new_values=new_values, old_values=old_values)

    @staticmethod
    def remap_value(input, param_type):
        """
        Because some values should not be returned as-is
        """
        if param_type == PrehistoryEntry.Attr.CMS_ACTIVITY:
            return input and input.isdigit() and ActivityResolver.name_by_id(int(input)) or None
        elif param_type == PrehistoryEntry.Attr.INST_CODE:
            return SupersedersResolver.get_present_value(Institute.code, input)
        return input


class PrehistoryFromPh(PrehistoryEntry):

    pattern = r'iCMSfor(?P<who_id>\d+)_(?P<date>\d{2}/\d{2}/\d{4})-fromPH:(?P<what>.+)'
    pattern_details = r'(?:and)?(\w+)=(\w+)(?=and-|and|$)'

    def __init__(self, date, new_values, old_values=None):
        self.date = date
        self.new_values = new_values
        self.old_values = old_values

    @classmethod
    def from_match(cls, match):
        what = match.group('what')
        new_values = {}
        for key, val in re.findall(PrehistoryFromPh.pattern_details, what):
            new_values[PrehistoryEntry.Attr.resolve(key)] = val
        return cls(date=PrehistoryEntry.make_date(match.group('date')), new_values=new_values)


class PrehistoryRegistration(PrehistoryEntry):

    # in project (?P<project>\w+)? has been taken out - it does not always occur
    # update: the relevant info actually occurs later in the line, like: CBI approved, Activity = Doctoral Student, M&O = , Project = HC
    pattern = r'as (?P<initial_activity_name>.+).* in institute (?P<inst>[A-Za-z\_\-0-9]+) .*(CBI approved, Activity = )(?P<activity_name>[^,]+),(.*?, Project = (?P<project>\w+).+?)?.*(?P<date>\d{2}/\d{2}/\d{4}) \d{2}:\d{2}IDONE , approvedBySecr'

    __old_activity_names = {
        'Undergraduate Student': 'Non-Doctoral Student',
        'Graduate Student': 'Doctoral Student'
    }

    def __init__(self, date, new_values, old_values=None):
        self.date = date
        self.new_values = new_values
        self.old_values = old_values

    @classmethod
    def from_match(cls, match):
        new_values = dict()
        new_values[PrehistoryEntry.Attr.CMS_ACTIVITY] = cls.__old_activity_names.get(*([match.group('activity_name')]*2))
        new_values[PrehistoryEntry.Attr.CMS_STATUS] = 'CMS'
        new_values[PrehistoryEntry.Attr.INST_CODE] = SupersedersResolver.get_present_value(Institute.code, match.group('inst'))
        new_values[PrehistoryEntry.Attr.PROJECT] = match.group('project')
        return cls(date=PrehistoryEntry.make_date(match.group('date')), new_values=new_values)


class PrehistoryRegistrationShort(PrehistoryRegistration):
    pattern = r'(Submitted by Secretariat)* .* as (?P<activity_name>[A-Z][A-Za-z -]+?) in (project (?P<project>[A-Za-z\_\-0-9]+) in )?(institute (?P<inst>[A-Za-z\_\-0-9]+)) .*(?P<date>\d{2}/\d{2}/\d{4}) \d{2}:\d{2}IDONE(:? , approvedBySecr)?'
