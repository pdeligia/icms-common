import pytest
from icmsutils.icmstest.mock import OldIcmsMockPersonFactory, MockUserFactory
from scripts.sync import ForwardSyncManager, PeopleSyncAgent, CareerEventsSyncAgent
from datetime import date
from icmsutils.icmstest.assertables import count_in_db, assert_attr_val
from icms_orm.cmspeople import Person
from icms_orm.common import Person as Person2, CareerEvent, CareerEventTypeValues


class TestData():
    overrides = [{
        Person.cmsId: 1,
        Person.dateRegistration: date(1900, 1, 1)
    }, {
        Person.cmsId: 43,
        Person.dateRegistration: date(2001, 6, 1)
        
    }, {
        Person.cmsId: 7981,
        Person.dateRegistration: date(2011, 2, 28),
        Person.exDate: '2020-3-31'
    }]

@pytest.fixture(scope='class')
def insert_people():
    OldIcmsMockPersonFactory.reset_overrides()
    for overrides_dict in TestData.overrides:
        _p = OldIcmsMockPersonFactory.create_instance(instance_overrides=overrides_dict)
        Person.session().add(_p)
        Person.session().add(MockUserFactory.create_instance(person=_p))
    Person.session().commit()

@pytest.fixture
def update_people():
    p:Person = Person.query.get(43)
    p.exDate = '2019-12-01'
    p.session().add(p)
    p.session().commit()

@pytest.fixture
def sync_dbs():
    ForwardSyncManager.launch_sync(agent_classes_override=[PeopleSyncAgent, CareerEventsSyncAgent])

@pytest.mark.usefixtures('recreate_common_db', 'recreate_people_db', 'insert_people')
class TestCareerEventsSync():
   
    @pytest.mark.parametrize('cms_id,arrival,departure',[(o.get(Person.cmsId), o.get(Person.dateRegistration), o.get(Person.exDate)) for o in TestData.overrides])
    def test_initial_sync(self, sync_dbs, cms_id, arrival, departure):
        assert 3 == count_in_db(Person2)
        assert 4 == count_in_db(CareerEvent)
        assert_attr_val(CareerEvent, {CareerEvent.cms_id: cms_id, CareerEvent.event_type: CareerEventTypeValues.CMS_ARRIVAL}, {CareerEvent.date: arrival})
        if departure is None:
            assert 0 == count_in_db(CareerEvent, {CareerEvent.cms_id: cms_id, CareerEvent.event_type: CareerEventTypeValues.CMS_DEPARTURE})
        else:
            departure_date:date = Person.from_ia_dict({Person.exDate: departure}).dateDeparture
            assert_attr_val(CareerEvent, {CareerEvent.cms_id: cms_id, CareerEvent.event_type: CareerEventTypeValues.CMS_DEPARTURE}, {CareerEvent.date: departure_date})

    @pytest.mark.parametrize('cms_id,departure', [(43, date(2019, 12, 1))])
    def test_updated_departures(self, update_people, sync_dbs, cms_id, departure):
        assert 3 == count_in_db(Person2)
        assert 2 == count_in_db(CareerEvent, {CareerEvent.event_type: CareerEventTypeValues.CMS_DEPARTURE})
        assert_attr_val(CareerEvent, {CareerEvent.event_type: CareerEventTypeValues.CMS_DEPARTURE, CareerEvent.cms_id: cms_id}, {CareerEvent.date: departure})
