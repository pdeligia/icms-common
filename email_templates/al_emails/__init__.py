from email_templates.email_template import EmailTemplateBase


class EmailTemplateAuthorListAddPerson(EmailTemplateBase):
    _sender = '{sender_email}'
    _recipients = '{author_email}'
    _cc = 'cms-ab-support@cern.ch, icms-support@cern.ch'
    _subject = 'Change in author list for {al_code}'
    _body = '''{sign_name} has been added to the author list of {inst_code}

    Regards,
    {sender_signature}'''

    def __init__(self, al_code, sign_name, author_email, inst_code, sender_signature, sender_email):
        super(EmailTemplateAuthorListAddPerson, self).__init__(al_code=al_code, sign_name=sign_name,
            inst_code=inst_code, sender_signature=sender_signature, author_email=author_email,
                                                               sender_email=sender_email)

